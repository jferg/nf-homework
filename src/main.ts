import "reflect-metadata";
// Due to an issue in the current release of Commander, this has to be imported using a
// default import.  More at: https://github.com/tj/commander.js/issues/1465
import * as commander from "commander";
import { AzureMapsRestService } from "./services/azure.maps.rest.service";
import { YelpRestService } from "./services/yelp.rest.service";
import { createConnection, getRepository, In, Repository } from "typeorm";
import { BusinessEntity } from "./entity/business";
import { Business } from "./models/business";
import debug from "debug";

const debugLog = debug("nfhw:debug");

/**
 * Main Application Class
 *
 * We utilize `commander` to handle commandline argument parsing, and for some other niceties for developing
 * CLI applications in JS/TS.  We use TypeORM for our database access layer - it's probably overkill for this
 * specific use case, but it works well and if we were ever to expand this info a bigger system it would make
 * it simpler.
 */
class RestaurantFinderApp {
  private app: commander.Command;
  private options: Record<string, any>;
  private azureService: AzureMapsRestService;
  private yelpService: YelpRestService;
  private businessRepository: Repository<Business>;

  constructor(args: string[]) {
    this.app = new commander.Command();
    this.app.version("0.0.1");

    this.app
      .description(
        "A simple CLI for finding highest-rated businesses of a specific type on a route between two locations."
      )
      // .option("--start", "Starting address")
      // .option("--end", "Ending address")
      .option("--max-detour-time <seconds>", "Maximum detour time from route, in seconds.", "900")
      // .option("--max-results", "Maximum number of results to return.")
      .option(
        "--type <type>",
        "Business type - i.e. 'mexican restaurant', 'diner', 'grocery store'",
        "restaurant"
      )
      .parse();

    this.options = this.app.opts();

    this.azureService = new AzureMapsRestService();
    this.yelpService = new YelpRestService();
  }

  /**
   * Uses the Azure maps rest service to find a route between two points, and then find businesses matching
   * the search criteria along that route, then stores them in the database.
   *
   * @param search
   * @param start
   * @param end
   * @param maximumDetourTime
   */
  async findAndSaveBusinessesAlongRoute(
    search: string,
    start: number[],
    end: number[],
    maximumDetourTime?: number
  ) {
    debugLog("findAndSaveBusinessesAlongRoute");
    const route = await this.azureService.getRouteBetweenPoints(start, end);

    const foundBusinesses: Array<string> = [];

    // Get our list of businesses along the route from Azure maps.
    const businessesAlongRoute = await this.azureService.getBusinessesAlongRoute(
      route,
      search,
      maximumDetourTime
    );

    for (const business of businessesAlongRoute) {
      debugLog("Searching for existing entry.");
      const res = await this.businessRepository.findOne({
        where: {
          azureId: business.azureId,
        },
        loadEagerRelations: true,
      });
      if (res) {
        foundBusinesses.push(res.id);
        // await this.businessRepository.update(res.id, business);
      } else {
        let res = await this.businessRepository.save(business);
        foundBusinesses.push(res.id);
      }
    }

    debugLog("/findAndSaveBusinessesAlongRoute");
    return foundBusinesses;
  }

  /**
   * Update each business in the passed-in array of businessIds with yelp data.
   *
   * @param businessIds
   */
  async updateBusinessRatings(businessIds: Array<string>) {
    debugLog("updateBusinessRatings");

    const savedBusinesses = await this.businessRepository.find({
      where: {
        id: In([...businessIds]),
      },
      loadEagerRelations: true,
    });

    for (const business of savedBusinesses) {
      // If the business doesn't have a yelpId or a rating populated, we need to look it up.
      if (!business.yelpId || !business.rating) {
        let matched: Business;

        try {
          const yelpBusinesses: Array<Business> = await this.yelpService.matchBusiness(business);
          if (yelpBusinesses.length > 1) {
            console.warn("Found more than one match for business in Yelp.  Using first match.");
          }
          matched = yelpBusinesses[0];
        } catch (e) {
          console.error("Error matching business in Yelp: ", e);
        }

        // Lazily we're just using the Yelp rating as our key to decide whether
        // or not we need to look up the Yelp details here.  This logic could
        // definitely be more comprehensive.
        if (!matched) {
          console.warn(`Failed to match "${business.name}" with Yelp.`);
        } else {
          try {
            const businessDetail = await this.yelpService.getBusinessDetails(matched);
            // Merge the existing record with matched and detailed records and save back to the database.
            const detailedBusiness = this.businessRepository.merge(
              business,
              matched,
              businessDetail
            );
            await this.businessRepository.save(detailedBusiness);
          } catch (e) {
            console.error("Error getting and saving business details: ", e);
          }
        }
      }
      debugLog("/updateBusinessRatings");
    }
  }

  /**
   * Runs a database query to find the 5 highest rated (and not closed) businesses in the database that are included in
   * `businessIds`, ordered by rating and then by number of reviews.
   *
   * @param businessIds
   */
  async findHighestRatedBusinesses(businessIds: Array<string>): Promise<Array<Business>> {
    debugLog("findHighestRatedBusinesses /");

    return await this.businessRepository
      .createQueryBuilder("business")
      .leftJoinAndSelect("business.location", "location")
      .where("business.id IN (:...ids)", { ids: businessIds })
      .andWhere("business.isClosed = false")
      .orderBy("business.rating", "DESC")
      .addOrderBy("business.reviewCount", "DESC")
      .limit(5)
      .getMany();
  }

  /**
   * Displays the results in a somewhat user-friendly output format.
   *
   * @param businesses
   */
  outputResults(businesses: Array<Business>) {
    console.log("\nRESULTS:\n");
    for (const business of businesses) {
      console.log(`  ${business.name} (${business.categories})`);
      console.log(`  Rating: ${business.rating} (${business.reviewCount} reviews)`);
      console.log(`  Address: ${business.location.address1}`);
      business.location.address2 && console.log(`           ${business.location.address2}`);
      business.location.address3 && console.log(`           ${business.location.address3}`);
      console.log(
        `           ${business.location.city}, ${business.location.state} ${business.location.postalCode}`
      );
      console.log(``);
    }
  }

  /**
   * This is the main flow of the application.
   */
  async run() {
    debugLog("run");
    // Hard-coded start and end points for the time being.
    // TODO: Implement "start" and "end" address searching.
    const start: number[] = [-119.990922, 39.557847];
    const end: number[] = [-120.0135338, 38.9227254];

    console.log("Connecting to database.");

    await createConnection();
    this.businessRepository = getRepository(BusinessEntity);

    console.log("Searching for businesses along route.");
    const foundBusinesses = await this.findAndSaveBusinessesAlongRoute(
      this.options.type,
      start,
      end
    );

    console.log("Fetching ratings for businesses.");
    await this.updateBusinessRatings(foundBusinesses);

    console.log("Finding best-rated businesses.");
    const bestRated = await this.findHighestRatedBusinesses(foundBusinesses);

    this.outputResults(bestRated);

    debugLog("/run");
  }
}

/**
 * Instantiate our app, and run it.
 */
(async () => {
  let app = new RestaurantFinderApp(process.argv);

  await app.run();
})();
