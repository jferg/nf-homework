import BaseObject from "./base";
import { Location } from "./location";

export interface Business extends BaseObject {
  name?: string;

  isClosed?: boolean;
  rating?: number;
  reviewCount?: number;
  price?: string;
  categories?: string[];
  yelpId?: string;
  yelpUrl?: string;
  azureId?: string;

  location?: Location;
}
