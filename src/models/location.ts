/**
 * Simplified model to represent a location, either of a business, or of a starting or ending point.
 */
import BaseObject from "./base";
import { Business } from "./business";

export interface Location extends BaseObject {
  address1: string;
  address2?: string;
  address3?: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;

  latitude?: number;
  longitude?: number;

  businesses: Array<Business>;
}
