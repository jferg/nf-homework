export default interface BaseObject {
  id: string;
  createTimestamp: Date;
  modifyTimestamp: Date;
}
