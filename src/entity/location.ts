import { Location } from "../models/location";
import { EntitySchema } from "typeorm";
import { BaseEntityColumns } from "./base-entity";

export const LocationEntity = new EntitySchema<Location>({
  name: "location",

  columns: {
    ...BaseEntityColumns,
    address1: {
      type: "text",
      nullable: true,
    },
    address2: {
      type: "text",
      nullable: true,
    },
    city: {
      type: "text",
      nullable: true,
    },
    state: {
      type: "text",
      nullable: true,
    },
    postalCode: {
      type: "text",
      nullable: true,
    },
    country: {
      type: "text",
      default: "US",
      nullable: true,
    },
    latitude: {
      type: "float",
      nullable: true,
    },
    longitude: {
      type: "float",
      nullable: true,
    },
  },

  relations: {
    businesses: {
      eager: false,
      type: "one-to-many",
      target: "business",
      inverseSide: "location",
    },
  },
});
