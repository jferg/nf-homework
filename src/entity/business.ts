import { Business } from "../models/business";
import { EntitySchema } from "typeorm";
import { BaseEntityColumns } from "./base-entity";

export const BusinessEntity = new EntitySchema<Business>({
  name: "business",

  columns: {
    ...BaseEntityColumns,
    name: {
      type: "text",
    },
    isClosed: {
      type: "boolean",
      default: false,
    },
    rating: {
      type: "float",
      nullable: true,
    },
    reviewCount: {
      type: "int",
      nullable: true,
    },
    price: {
      type: "float",
      nullable: true,
    },
    categories: {
      type: "text",
      nullable: true,
    },
    yelpId: {
      type: "text",
      nullable: true,
    },
    yelpUrl: {
      type: "text",
      nullable: true,
    },
    azureId: {
      type: "text",
      nullable: true,
    },
  },

  relations: {
    /* This is a many-to-one relation because it's likely we'll encounter multiple businesses at the
       same location.  (There could also be multiple locations for a given business, but yelp treats those
       as independent businesses, from what I can tell.)
     */
    location: {
      eager: true,
      cascade: true,
      type: "many-to-one",
      target: "location",
      inverseSide: "businesses",
    },
  },
});
