import { EntitySchemaColumnOptions } from "typeorm";

export const BaseEntityColumns = {
  id: {
    type: "varchar",
    primary: true,
    generated: "uuid",
    nullable: true,
  } as EntitySchemaColumnOptions,
  createTimestamp: {
    createDate: true,
    type: "date",
  } as EntitySchemaColumnOptions,
  modifyTimestamp: {
    updateDate: true,
    type: "date",
  } as EntitySchemaColumnOptions,
};
