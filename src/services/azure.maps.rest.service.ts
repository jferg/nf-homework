import * as azMaps from "azure-maps-rest";
import debug from "debug";

import SearchAlongRouteResult = azMaps.Models.SearchAlongRouteResult;
import SearchResultAddress = azMaps.Models.SearchResultAddress;
import CoordinateAbbreviated = azMaps.Models.CoordinateAbbreviated;

import { MultiLineString, LineString, Position } from "geojson";
import { Business } from "../models/business";
import { Location } from "../models/location";

const { azureMapsApiKey } = require("../../config.json");
const debugLog = debug("nfhw:azMaps");

/**
 * AzureMapsRestService Provides a simple interface to the Azure maps service to be consumed
 * by the application.
 *
 * More info on the Rest API:
 *
 * https://docs.microsoft.com/en-us/rest/api/maps/search/post-search-along-route
 * https://docs.microsoft.com/en-us/azure/azure-maps/azure-maps-authentication
 *
 */
export class AzureMapsRestService {
  private readonly subscriptionKeyCredential;
  private readonly authPipeline: azMaps.Pipeline;
  private readonly routeUrl: azMaps.RouteURL;
  private readonly searchUrl: azMaps.SearchURL;

  /**
   * Converts an Azure business search result to an application Business object.
   *
   * @param business An individual business result from a search along a route.
   */
  public static searchResultToBusiness(business: SearchAlongRouteResult): Business {
    debugLog("searchResultToBusiness /");
    return {
      name: business.poi.name,
      categories: business.poi.categories,
      azureId: business.id,
      location: AzureMapsRestService.addressAndPositionToLocation(
        business.address,
        business.position
      ),
    } as Business;
  }

  /**
   * Converts the address and position elements of a search result to an application Location
   * object.
   *
   * @param address
   * @param position
   */
  public static addressAndPositionToLocation(
    address: SearchResultAddress,
    position: CoordinateAbbreviated
  ) {
    debugLog("addressAndPositionToLocation");
    return {
      address1: `${address.streetNumber} ${address.streetName}`,
      city: address.municipality,
      state: address.countrySubdivision,
      postalCode: address.postalCode,
      latitude: position.lat,
      longitude: position.lon,
    } as Location;
  }

  /**
   * Creates a new AzureMapsRestService instance, and initializes the Azure-Maps-Rest
   * authentication and URLs.
   */
  constructor() {
    this.subscriptionKeyCredential = new azMaps.SubscriptionKeyCredential(azureMapsApiKey);

    this.authPipeline = azMaps.MapsURL.newPipeline(this.subscriptionKeyCredential, {
      retryOptions: { maxTries: 4 },
    });

    this.routeUrl = new azMaps.RouteURL(this.authPipeline);
    this.searchUrl = new azMaps.SearchURL(this.authPipeline);
  }

  /**
   * Searches and returns a route between two points.
   *
   * @param startingPoint
   * @param endingPoint
   */
  async getRouteBetweenPoints(startingPoint: Position, endingPoint: Position): Promise<LineString> {
    const directions = await this.routeUrl.calculateRouteDirections(azMaps.Aborter.none, [
      startingPoint,
      endingPoint,
    ]);

    // To be difficult, calculateRouteDirections returns a MultiLineString ... but
    // searchAlongRoute wants a LineString.  There's probably a better way to convert
    // from one to the other but the documentation for Azure maps is terrible and this
    // gets the job done.
    const mlsRoute: MultiLineString = directions.geojson.getFeatures().features[0].geometry;

    return {
      type: "LineString",
      coordinates: mlsRoute.coordinates[0],
      bbox: mlsRoute.bbox,
    };
  }

  /**
   * Searches the Azure API for businesses matching a given search criteria within
   * maxDetourTime seconds of the provided route.
   *
   * @param route The route, as a GeoJSON LineString (array of points).
   * @param searchString The search criteria - i.e.
   * @param maxDetourTime Maximum detour time (in seconds) - default 900 (15 minutes).
   * @returns Array<Business> List of Businesses found along route.
   */
  async getBusinessesAlongRoute(
    route: LineString,
    searchString: string,
    maxDetourTime: number = 900
  ): Promise<Array<Business>> {
    debugLog("getBusinessesAlongRoute");
    const request: azMaps.Models.SearchAlongRouteRequestBody = {
      route: route,
    };

    const businesses = await this.searchUrl.searchAlongRoute(
      azMaps.Aborter.none,
      searchString,
      maxDetourTime,
      request,
      { limit: 20 } // Unfortunately 20 items is the most we can return.
    );

    debugLog("/getBusinessesAlongRoute");

    return businesses.results.map((business) =>
      AzureMapsRestService.searchResultToBusiness(business)
    );
  }
}
