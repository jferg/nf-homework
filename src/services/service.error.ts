import { AxiosResponse } from "axios";

export class ServiceError extends Error {
  constructor(response?: AxiosResponse | Error) {
    let message: string;

    if (response && !(response instanceof Error)) {
      message = `${response.status}: ${response.statusText} ${
        response.data ? `(${response.data})` : ""
      }`;
    }

    if (response && response instanceof Error) {
      super(response.message);
    } else {
      super(message);
    }
  }
}
