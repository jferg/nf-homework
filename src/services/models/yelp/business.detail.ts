/**
 * YelpBusinessDetail and ancillary classes.  Implements a local version of the Yelp Business
 * Detail response body as described at https://www.yelp.com/developers/documentation/v3/business
 * along with documentation of all the fields.
 */

import { YelpBusiness } from "./business";
import { plainToClass } from "class-transformer";

export class YelpCategory {
  alias?: string;
  title?: string;
}

export class YelpHours {
  open?: Array<YelpDailyHours>;
  hours_type?: string;
  is_open_now?: boolean;
}

export class YelpDailyHours {
  start?: string;
  end?: string;
  day?: number;
  is_overnight?: boolean;
}

export class YelpSpecialHours {
  date?: string;
  is_closed?: boolean;
  start?: string;
  end?: string;
  is_overnight?: string;
}

export class YelpBusinessDetailResponse extends YelpBusiness {
  image_url?: string;
  is_claimed?: boolean;
  is_closed?: boolean;
  url?: string;
  display_phone?: string;
  review_count?: number;
  rating?: number;
  categories?: Array<YelpCategory>;
  photos?: Array<string>;
  price?: string;
  hours?: YelpHours;
  specialHours?: YelpSpecialHours;

  static fromObject(obj: Partial<YelpBusinessDetailResponse>) {
    return plainToClass(YelpBusinessDetailResponse, obj);
  }
}
