import { YelpBusiness } from "./business";
import { Business } from "../../../models/business";
import {
  classToPlain,
  Exclude,
  Expose,
  plainToClass,
  Type,
} from "class-transformer";

export class YelpBusinessMatchRequest {
  name: string;
  address1: string;
  address2?: string;
  address3?: string;
  city: string;
  state: string;
  country: string;
  latitude?: number;
  longitude?: number;
  phone?: string;
  zip_code?: string;
  yelp_business_id?: string;
  limit?: number;
  match_threshold?: "none" | "default" | "strict";

  /**
   * Takes a Business object (or something shaped like one) and returns a YelpBusinessMatchRequest
   * object which can be used to match that business in the Yelp api.
   *
   * @param business
   */
  static fromBusiness(business: Business): YelpBusinessMatchRequest {
    return Object.assign(new YelpBusinessMatchRequest(), {
      name: business.name,
      address1: business.location?.address1,
      address2: business.location?.address2,
      address3: business.location?.address3,
      city: business.location?.city,
      state: business.location?.state,
      country: business.location?.country,
      latitude: business.location?.latitude,
      longitude: business.location?.longitude,
    });
  }
}

export class YelpBusinessMatchResponse {
  @Type(() => YelpBusiness)
  businesses?: Array<YelpBusiness>;

  static fromObject(plainObject: any) {
    return plainToClass(YelpBusinessMatchResponse, plainObject);
  }
}
