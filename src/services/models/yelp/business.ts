/**
 * Core business object for Yelp business responses, and ancillary locational coordinates.
 * This object is the base for both YelpBusinessDetailResponse and (as an array) for
 * YelpMatchResponse.
 */
import { Business } from "../../../models/business";

export class YelpLocation {
  address1?: string;
  address2?: string;
  address3?: string;
  city?: string;
  zip_code?: string;
  country?: string;
  state?: string;
  displayAddress?: Array<string>;
  cross_streets?: string;
}

export class YelpCoordinates {
  latitude?: number;
  longitude?: number;
}

export class YelpBusiness {
  id: string;
  alias?: string;
  name?: string;
  location?: YelpLocation;
  coordinates?: YelpCoordinates;
  phone?: string;
}
