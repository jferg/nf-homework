import debug from "debug";
import { YelpBusinessMatchRequest, YelpBusinessMatchResponse } from "./models/yelp/business.match";
import { Business } from "../models/business";
import { YelpBusinessDetailResponse } from "./models/yelp/business.detail";
import * as config from "../../config.json";
import axios, { AxiosInstance } from "axios";
import { ServiceError } from "./service.error";
import { YelpBusiness, YelpCoordinates, YelpLocation } from "./models/yelp/business";
import { Location } from "../models/location";

const debugLog = debug("nfhw:yelp");

const BUSINESS_DETAILS_ENDPOINT = "/businesses";
const BUSINESS_MATCH_ENDPOINT = "/businesses/matches";

export class YelpRestService {
  private readonly yelpClientId: string;
  private readonly yelpApiKey: string;
  private readonly yelpApiEndpoint: string;

  private httpClient: AxiosInstance;

  public static yelpLocationAndCoordinatesToLocation(
    location: YelpLocation,
    coordinates: YelpCoordinates
  ): Location {
    debugLog("yelpLocationAndCoordinatesToLocation /");
    return {
      address1: location.address1,
      address2: location.address2,
      address3: location.address3,
      city: location.city,
      postalCode: location.zip_code,
      state: location.state,
      country: location.country,
      latitude: coordinates.latitude,
      longitude: coordinates.longitude,
    } as Location;
  }

  public static yelpBusinessToBusiness(business: YelpBusiness): Business {
    debugLog("yelpBusinessToBusiness /");
    return {
      name: business.name,
      yelpId: business.id,
      location: YelpRestService.yelpLocationAndCoordinatesToLocation(
        business.location,
        business.coordinates
      ),
    } as Business;
  }

  /**
   * Converts a YelpBusinessDetailResponse to a local 'Business' object for storage in
   * the database.
   *
   * @param business The YelpBusinessDetailResponse to convert.
   * @returns Business An application Business object.
   */
  public static yelpBusinessDetailToBusiness(business: YelpBusinessDetailResponse): Business {
    debugLog("yelpBusinessDetailToBusiness /");
    return {
      name: business.name,
      isClosed: business.is_closed,
      rating: business.rating,
      reviewCount: business.review_count,
      price: business.price,
      categories: business.categories?.map((c) => c.title),
      yelpId: business.id,
      yelpUrl: business.url,
      location: YelpRestService.yelpLocationAndCoordinatesToLocation(
        business.location,
        business.coordinates
      ),
    } as Business;
  }

  constructor() {
    this.yelpApiEndpoint = config.yelpApiEndpoint;
    this.yelpApiKey = config.yelpApiKey;
    this.yelpClientId = config.yelpClientId;

    /* Set up our axios instance with the correct base URL and authorization header. */
    this.httpClient = axios.create({ baseURL: this.yelpApiEndpoint });
    this.httpClient.defaults.headers.common["Authorization"] = `Bearer ${this.yelpApiKey}`;
  }

  /**
   * Matches a partial business record (populated from Azure Maps) with a Yelp business
   * and returns an object containing 0 or more matched YelpBusinesses.
   *
   * @param business
   */
  async matchBusiness(business: Business): Promise<Array<Business>> {
    debugLog("matchBusiness");
    const businessQuery = YelpBusinessMatchRequest.fromBusiness(business);
    let result;

    try {
      result = await this.httpClient.get(BUSINESS_MATCH_ENDPOINT, {
        params: businessQuery,
      });
    } catch (e) {
      throw new ServiceError(e);
    }

    if (result.status !== 200) {
      throw new ServiceError(result);
    }

    const matches = YelpBusinessMatchResponse.fromObject(result.data);

    debugLog("/matchBusiness");
    return matches.businesses.map((b) => YelpRestService.yelpBusinessToBusiness(b));
  }

  /**
   * Queries and returns details for the provided Business.
   *
   * @param business
   */
  async getBusinessDetails(business: Business): Promise<Business> {
    debugLog("getBusinessDetails");
    if (!business.yelpId) {
      throw new ServiceError(new Error("Business has no yelpId, cannot fetch details."));
    }
    const queryUrl = `${BUSINESS_DETAILS_ENDPOINT}/${business.yelpId}`;
    const result = await this.httpClient.get(queryUrl);

    if (result.status !== 200) {
      throw new ServiceError(result);
    }

    debugLog("/getBusinessDetails");
    return YelpRestService.yelpBusinessDetailToBusiness(
      YelpBusinessDetailResponse.fromObject(result.data)
    );
  }
}
