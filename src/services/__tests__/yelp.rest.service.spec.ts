import { YelpRestService } from "../yelp.rest.service";
import { Business } from "../../models/business";
import { Location } from "../../models/location";
import axios, { AxiosResponse } from "axios";
import * as BusinessDetailsResponse from "../__fixtures__/business.details.api.response.json";
import * as BusinessMatchResponse from "../__fixtures__/business.match.api.response.json";

import ResolvedValue = jest.ResolvedValue;

jest.mock("axios");

const { get } = axios as jest.Mocked<typeof import("axios").default>;

describe("yelp rest api service", () => {
  describe("matchBusiness", () => {
    it("successfully matches a business in the yelp API", async () => {
      const mockedResponse: AxiosResponse = {
        data: BusinessMatchResponse,
        status: 200,
        statusText: "OK",
        headers: {},
        config: {},
      };

      get.mockResolvedValue(mockedResponse as ResolvedValue<unknown>);

      const service = new YelpRestService();
      const business = {
        location: {
          address1: "800 N Point St",
          city: "San Francisco",
          state: "CA",
        } as Location,
      } as Business;
      const result: Business[] = await service.matchBusiness(business);
      expect(result).not.toBeUndefined();
      // TODO
    });
  });

  describe("businessDetails", () => {
    it("successfully queries the yelp API for business details", async () => {
      const mockedResponse: AxiosResponse = {
        data: BusinessDetailsResponse,
        status: 200,
        statusText: "OK",
        headers: {},
        config: {},
      };

      get.mockResolvedValue(mockedResponse as ResolvedValue<unknown>);

      const service = new YelpRestService();
      const business = { yelpId: "WavvLdfdP6g8aZTtbBQHTw" } as Business;
      const result: Business = await service.getBusinessDetails(business);

      expect(result).not.toBeUndefined();
      expect(result.categories?.length).toBe(3);
      expect(result.yelpUrl).toBe(
        "https://www.yelp.com/biz/gary-danko-san-francisco?adjust_creative=wpr6gw4FnptTrk1CeT8POg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=wpr6gw4FnptTrk1CeT8POg"
      );
      expect(result.name).toBe("Gary Danko");
      expect(result.isClosed).toBeFalsy();
    });
  });
});
