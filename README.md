
## FoodFinder

This is a small commandline application which uses the Azure maps API and the Yelp reviews API to find a route between two points, and then identify the 5 best restaurants (businesses, really) within a given time's detour from that route.  

I wanted to create something that might actually useful to me with this exercise, and since we will be taking a vacation next month, I wanted to know where we should stop to eat.  

Currently the starting and ending points are hard-coded latitudes and longitudes, but things are stubbed in to be able to provide addresses on the commandline instead.  (Current start/end points are Reno, NV to South Lake Tahoe, CA.)

There are some limitations imposed by the Azure API - namely that the "search along route" API call will only return a maximum of 20 responses, which makes the real-world usefulness of this script limited, but it's still a fun little tool. 

### Running It

To run the script:
```bash
    npm install
    npm start -- --type chinese                             # Find the best chinese restaurants within a 15 minute detour
    npm start -- --max-detour-time 300 --type "fast food"   # Find the best fast food places within a 5-minute detour.
    npm start -- --help
```

Debug logging can be enabled by setting the `DEBUG` environment variable to `nfhw:*`.  Currently the debug log primarily shows entry and exit of the various functions.

#### Running Tests

```bash
    npm install
    npm test
```

### Architecture

The script is TypeScript and uses [TypeORM](https://typeorm.io/) for database access.  It is configured out of the box to use a SQLite3 database because it's simple and doesn't involve a separate server.  This can be changed by editing `ormconfig.json`.
The ORM models and entities are  in `src/models` and `src/entities`, respectively.

The core application logic is in `src/main.js`.  There are two service abstractions in `src/services` - one for the Yelp service, and one for the Azure Maps service.  The Azure Maps service is a wrapper around the [`azure-maps-rest`](https://www.npmjs.com/package/azure-maps-rest) NPM module, while the Yelp service uses `axios` to perform REST calls directly.

There should be clear separation of duty between the main script and the services - everything in the main script utilizes our local `Business` and `Location` abstractions - there should be no visibility to the API-provider-specific data structures - and the services should be unaware of the existence of the database.

The basic flow of the process is: 

1. Query Azure for our route. 
2. Query Azure for businesses along route, and store the results in the database. Return the database keys of the found results. 
3. Query the database for the found keys.  If the businesses have already been looked up in a previous run, the yelpId will be populated, so don't look them up again ... otherwise try to match the business from Azure to a business in Yelp. 
4. Assuming we successfully matched, get the details for the business from Yelp, and store them back to the database.  
5. Query the database for the highest rated records in this run's set, and display the resulting list.  

The database is not _really_ a necessary component, but it was a requirement for the "homework". 

### Limitations, Known Issues, and Future Enhancements

* Database structure could be optimized to use varchar with fixed length instead of string, etc. 
* Entities and models don't have tests, because there is no logic in them besides what is provided by TypeORM, which has its own tests.
* Overall, tests are sparse - the ones that are there are primarily for the purpose of showing what tests would look like if this were fleshed out to a full-fledged application.  Additionally, the `azure-maps-rest` module is poorly-documented and figuring out how to mock it is likely to take some time. 
* Only being able to get 20 results back from the Azure maps "Search Along Route" is limiting.   This could be worked around by finding the route, computing the distance between each set of points in the route, and then querying for businesses within "detour" minutes of every 1/2-"detour" points.  The database would come in handy here as we would end up getting back numerous duplicate results and could save ourselves from querying Yelp multiple times for duplicates.  
* Complete the ability to provide start/end addresses on the commandline.  
* Some logging output other than the final result (and debug logging) to let the user know how the run is progressing would be helpful. 
* Storing the route and search criteria in the database to save us re-searching along the route would save some API queries.  
* Adding in checks for whether the businesses returned are open at a given time would be a nice feature.   
